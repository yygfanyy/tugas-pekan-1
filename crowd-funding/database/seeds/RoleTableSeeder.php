<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        DB::table('roles')->insert([
            'id' => $faker->uuid,
            'nama_role' => 'Admin',
        ]);
        DB::table('roles')->insert([
            'id' => $faker->uuid,
            'nama_role' => 'User',
        ]);
    }
}
