<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class OtpTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        DB::table('otp_codes')->insert([
            'id' => $faker->uuid,
            'otp_code' => 'B81290',
            'valid_date' => $faker->dateTime,
        ]);
    }
}
