<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
 
        // insert data ke table users menggunakan Faker
        DB::table('users')->insert([
            'id' => $faker->uuid,
            'name' => $faker->name,
            'email' => $faker->email,
            'password' => bcrypt('password'),
            'roles_id' => 'a2659c88-f80e-3cbe-9c00-74f0da17623d',
            'otp_codes_id' => '48aeccc8-3456-3574-8719-5519a69d813b',
        ]);
        DB::table('users')->insert([
            'id' => $faker->uuid,
            'name' => $faker->name,
            'email' => $faker->email,
            'password' => bcrypt('password'),
            'roles_id' => '2d14810a-31ab-3505-8375-bff9b059f2d2',
            'otp_codes_id' => '48aeccc8-3456-3574-8719-5519a69d813b',
        ]);
    }
}
