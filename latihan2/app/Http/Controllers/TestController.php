<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class TestController extends Controller
{
    //proteksi route dengan middleware yg telah dibuat
    //CARA 1 :
    // public function __construct(){
    //     $this->middleware('dateMiddleware');
    // }

    public function test(){
        return 'Yuhuu ~ Kamu Berhasil Masuk';
    }
    public function test2(){
        return 'Annyeonghaseyo~ Kamu Berhasil Masuk ke Test2';
    }

    public function admin(){
        return 'Hallo Admin Cantik~';
    }
}
