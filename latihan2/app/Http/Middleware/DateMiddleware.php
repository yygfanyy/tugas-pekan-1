<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;

class DateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //cek tanggal sekarang
        $current_date = Carbon::now()->day;
        //logic utk mengakses hanya dari tanggal 11-20
        if($current_date >= 11 && $current_date <=20){
            //diteruskan ke controller yg dituju
            return $next($request);
        }
        abort(403);
    }
}
