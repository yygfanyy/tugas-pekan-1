<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//proteksi route dengan middleware yg telah dibuat
//CARA 2 :
Route::get('/test', 'TestController@test')->middleware('dateMiddleware');

//jika route yg ingin dihubungkan ke middleware banyak
Route::middleware('dateMiddleware')->group(function(){
    Route::get('/test', 'TestController@test');
    Route::get('/test2', 'TestController@test2');
});

Route::get('/admin', 'TestController@admin');

//buat authentication langkah-langkahnya sbb:
//1. composer require laravel/ui "^1.0" --dev
//2. php artisan ui vue --auth
//3. npm install
//4. npm run dev (fungsinya akan mencreate folder js dan css)

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
