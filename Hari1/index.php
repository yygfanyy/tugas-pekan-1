<?php

trait Hewan{
    public $darah = 50;
    public function atraksi(){
        echo $this->getName() . ' sedang '. $this->getKeahlian();
    }
    abstract public function getName();
    abstract public function getKeahlian();

    public function setDarah($darah){
        $this->darah = $darah;
        return $this;
    }
    public function getDarah(){
        return $this->darah;
    }
}
class Fight{
    public $attackPower;
    public $defensedPower;
    public $musuh = 'Harimau Sumatera';
    public $musuh2 = 'Elang Jawa';

    public function serang($musuh){
        echo $this->getName() . ' menyerang '. $this->musuh;
    }
    public function diserang($musuh2){
        echo $this->musuh2 . ' diserang '. $this->getName() . '<br>';
        echo 'sisa darah : ';
        echo $this->darah - $this->attackPower / $this->defensedPower;
    }

    public function serang2($musuh2){
        echo $this->getName() . ' menyerang '. $this->musuh2;
    }
    public function diserang2($musuh){
        echo $this->musuh . ' diserang '. $this->getName() . '<br>';
        echo 'sisa darah : ';
        echo $this->darah - $this->attackPower / $this->defensedPower;
    }
}

class Elang extends Fight{

    use Hewan;
    public $jmlKaki = 2;
    public $keahlian = 'Terbang Tinggi';
    public $attackPower = 10;
    public $defensedPower = 5;

    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name = $name;
        return $this;
    }
    public function getjmlKaki(){
        return $this->jmlKaki;
    }
    public function getKeahlian(){
        return $this->keahlian;
    }
    public function getAttack(){
        return $this->attackPower;
    }
    public function getDefensed(){
        return $this->defensedPower;
    }
    public function getInfoHewan(){
        echo 
        'Hewan '. $this->getName() . 
        ' Memiliki Darah '. $this->getDarah() . 
        ' Jumlah Kakinya '. $this->getjmlKaki() .
        ' Keahliannya adalah ' . $this->getKeahlian() . 
        ' dengan Attack Power '. $this->getAttack() . 
        ' dan Defensed Power ' . $this->getDefensed() ;
    }
}

class Harimau extends Fight{

    use Hewan;
    public $jmlKaki = 4;
    public $keahlian = 'Lari Cepat';
    public $attackPower = 7;
    public $defensedPower = 8;

    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name = $name;
        return $this;
    }
    public function getjmlKaki(){
        return $this->jmlKaki;
    }
    public function getKeahlian(){
        return $this->keahlian;
    }
    public function getAttack(){
        return $this->attackPower;
    }
    public function getDefensed(){
        return $this->defensedPower;
    }
    public function getInfoHewan(){
        echo 
        'Hewan '. $this->getName() . 
        ' Memiliki Darah '. $this->getDarah() . 
        ' Jumlah Kakinya '. $this->getjmlKaki() .
        ' Keahliannya adalah ' . $this->getKeahlian() . 
        ' dengan Attack Power '. $this->getAttack() . 
        ' dan Defensed Power ' . $this->getDefensed() ;
    }
}

$hewan1 = new Elang();
$hewan2 = new Harimau();
$hewan1->setName('Elang Jawa');
echo $hewan1->atraksi() . '<br>';
$hewan2->setName('Harimau Sumatera');
echo $hewan2->atraksi() . '<br>';
echo 
'-------------------------------'. '<br>';
//1. ELANG MENYERANG HARIMAU
echo $hewan1->serang($hewan2) . '<br>' ;
//2, HARIMAU DISERANG ELANG
echo $hewan2->diserang($hewan1) . '<br>' ;
echo 
'-------------------------------'. '<br>';
echo $hewan1->getInfoHewan(). '<br>';
echo 
'-------------------------------'. '<br>';
//3. HARIMAU MENYERANG ELANG
echo $hewan2->serang2($hewan1) . '<br>' ;
//4. ELANG DISERANG HARIMAU
echo $hewan1->diserang2($hewan2) . '<br>' ;
echo 
'-------------------------------'. '<br>';
echo $hewan2->getInfoHewan() . '<br>';
echo 
'-------------------------------';
?>